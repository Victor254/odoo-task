# -*- coding: utf-8 -*-

from openerp import models, fields, api

class employee_ref(models.Model):
    _name = 'employee_ref.employee_ref'

    name = fields.Char(required=True, string="Employee Name")
    email = fields.Char(required=True, string="Email")
    phone= fields.Integer(required=True,string="Phone No")
    previous_pos=fields.Char(required=True,string="Previous Position")

    #referee 1
    ref_1_name=fields.Char(required=True, string="Name")
    ref_1_email=fields.Char(required=True, string="Email")
    ref_1_phone=fields.Integer(required=True,string="Phone")
    ref_1_position=fields.Char(required=True,string="Position")

    #referee 2
    ref_2_name=fields.Char(required=True, string="Name")
    ref_2_email=fields.Char(required=True, string="Email")
    ref_2_phone=fields.Integer(required=True,string="Phone")
    ref_2_position=fields.Char(required=True,string="Position")

    #referee 3
    ref_3_name=fields.Char(required=True, string="Name")
    ref_3_email=fields.Char(required=True, string="Email")
    ref_3_phone=fields.Integer(required=True,string="Phone")
    ref_3_position=fields.Char(required=True,string="Position")



     #methods to be used
    @api.model
    def create(self,values):
      return super(employee_ref,self).create(values)
    
    @api.multi
    def write(self, values):
    
      return super(employee_ref, self).write(values)

    #@api.multi
    #def action_print_pdf(self):
     #   return True
        #return self.env['report'].get_action(self, 'employee_ref.employee_req_report')   

    

    
    